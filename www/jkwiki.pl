#!/usr/bin/perl

# JKwiki
# File jkwiki.pl
# Version 0.2

# &copy; 2015, Joey Kelly
# Written by Joey Kelly, http://joeykelly.net
# License: GPL Version 2

# Faced with the abandonment of KWiki by Ingy, I have to scrape something together to port my content over to a usable form,
# since I am trying to move off an old server and I can't get Kwiki to build correctly anymore.
# Therefore, with JKWiki and but a little modification of the data files, I can display them regardless of Kwiki status.
#
# Note: displaying the files is my primary concern at this time. Editing will come later.


use strict;
use warnings;

use feature qw(switch say);

use HTML::Template;
use HTML::Entities;

use Text::Tiki;


use lib qw(/home/oopaloompa/jkwiki/lib);

use JKWiki_Functions;


# disable buffering
$|=1;

my $r = shift;

$r->print ("Content-type: text/html\n\n");


# detect this, then we can throw up an instance wherever we please
# ___FIXME___ this is busted... we detect our path, yet set lib manually above (use the old BEGIN construct?)
my $scriptname  = $ENV{SCRIPT_FILENAME};
my @path        = split '/', $scriptname;
$scriptname     = pop @path;
pop @path;      # the database is outside the webserver's path, duh
my $pwd         = join '/', @path;
# if the above doesn't work, set the path to your files manually
#$pwd            = '/home/oopaloompa/public_html/jkwiki';
# what's your path? uncomment and find out ;-)
#say "path = $pwd<br>";



# want to change the look of the site? try these... but don't mess up the TEMPLATE tags or you'll break the site
my $template    = "$pwd/template";
my %template;
my @template    = qw(layout);
foreach my $tmpl (@template) {
  $template{$tmpl} = HTML::Template->new(filename => "$template/$tmpl.tmpl");
}

# here's where the wiki pages are
my $database   = "$pwd/database";
open my $pagelist, "ls $database |";

my $showdebug = 0;
#$showdebug++;

# GET
my $args = $r->args();
my @args;
@args = split('&',$args) if $args;
my %args;
foreach (@args) {
  my ($field,$value) = split('=',$_);
  $args{$field} = $value;
}
my @get = sort keys %args;

# POST
use Apache2::Request;
my $req = Apache2::Request->new($r);
my @params = $req->param;
if ($showdebug) {
  foreach (@params) {
    $r->print ("param $_ = " . $req->param("$_") . "<br>\n");
  }
}



# so let's try to process our wiki pages

my $content;
my $layout    = 'layout';
my $title     = 'JKWiki';
my $subtitle  = 'something';


my %page;
while (<$pagelist>) {
  chomp;
  $page{$_}++;
}

# can we figure out a random GET field?
my $wikipage;
if (@get) {
  foreach my $arg (@get) {
    if ($page{$arg}) {
      $wikipage = $arg;
      last;
    }
  }
} else {
  $wikipage = 'HomePage';    # no target? no problem
  # ___FIXME___ this doesn't account for missing data files, see below
}

if ($wikipage) {
  my $page =  JKWiki_Functions::parse_markup_file($database, $wikipage);
  $content .= $page;
} else {
  $content = "Quoth the raven,\n<h1 align=\"center\">404</h1>";
}

$subtitle  = $wikipage;


# LAYOUT
# now let's populate the proper layout template
$template{$layout}->param(TITLE     => $title     );
$template{$layout}->param(SUBTITLE  => $subtitle  );
$template{$layout}->param(CONTENT   => $content   );

# and flush everything to the browser
$r->print ($template{$layout}->output);
