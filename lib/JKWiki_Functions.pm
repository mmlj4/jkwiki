package JKWiki_Functions;

# written by Joey Kelly

use strict;
use warnings;


use Text::Tiki;
use HTML::Template;
use HTML::Entities;



# deprecate
sub slurp {
  my $file = shift;

  open my $fh, '<', $file or die;
  local $/ = undef;
  my $cont = <$fh>;
  close $fh;

  return $cont;
}



# deprecate
sub defang_html {
  my $html  = shift;

  $html = encode_entities($html);
  # let's selectively allow some few HTML tags...
  $html =~ s/\n/<br>/g;

  return $html;
}



# from JoKeShow
sub parse_markup_file {
  my ($database, $file) = @_;

  $file = safechars($file);
  open (my $fh, '<', "$database/$file") or return "Could not open file '$file' $!";
  my @markup;
  my $usecamelcase;
  $usecamelcase++;    # NOTE: we turn CamelCase markup on, regardless
  while (<$fh>) {
    my $line = safechars($_);
    if ($line =~ /^UseCamelCase/) {
      # if the first line of a database file has this string, we enable the old kwiki CamelCase markup, with a minor change (see the sub)
      $usecamelcase++;
      next;
    }
    # we want to make kwiki's CamelCase internal linking work here, with a minor compromise
    $line = kwiki_headings($line) if $line =~ /^=/;
    if ($usecamelcase) {
      $line = kwiki_camel_case($line) unless $line =~ /^[ \t]/;
    }
    push @markup, $line;
  }
  close $fh;
  my $html = '';
  my $tiki = Text::Tiki->new;
  $html =  $tiki->format(\@markup);

  return $html;
}




# subs

sub safechars {
  my ($string, $isgetarg) = @_;

  # NOTE: adjust as necessary
  if ($isgetarg) {
    $string =~ tr/a-zA-Z0-9//dc;
  } else {
    $string =~ tr/a-zA-Z0-9!@\#$%&*()[]=_+;':",.\/?| -//dc;            # English
  }

  return $string;
}



sub kwiki_camel_case {
  my $line = shift;

  chomp $line;   # why not?
  my @words = split /\s+/, $line;
  my @newline;
  foreach my $word (@words) {
    # so we have a two-phase design going on...
    # each database file must be marked as UseCamelCase (see the falling function),
    # and we force a leading ? character to actually trigger a CamelCase linking
    # it's a compromise... if we leave CamelCase as it stands, we get lots of false positives: e.g. UNO, IBM, etc.
    if ($word =~ /^\?([A-Z][a-z]*){2,}/) {
      my $text = $word;
      $text =~ s/\?//;
      $word = "[$text]:$word" 
    }
    push @newline, $word;
  }
  my $newline =  join ' ', @newline;

  return $newline;
}



sub kwiki_headings {
  my $line = shift;

  chomp $line;   # why not?
  if      ($line =~ /^====/)  {
    $line =~ s/^====//;
    $line =~ s/====$//;
    $line =~ s/^/!4/;
  } elsif ($line =~ /^===/)   {
    $line =~ s/^===//;
    $line =~ s/===$//;
    $line =~ s/^/!3/;
  } elsif ($line =~ /^==/)    {
    $line =~ s/^==//;
    $line =~ s/==$//;
    $line =~ s/^/!1/;
  } else                      {
    $line =~ s/^=//;
    $line =~ s/=$//;
    $line =~ s/^/!1/;
  }

  return $line;
}


1;
