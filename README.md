# JKWiki

A scraped-together wiki app, originally written to rescue old Kwiki data files.

## Nuts and Bolts

Faced with the abandonment of KWiki by Ingy, I have to scrape something together to port my content over to a usable form,  since I am trying to move off an old server and I can't get Kwiki to build correctly anymore. Therefore, with JKWiki and but a little modification of the data files, I can display them regardless of Kwiki status.

Note: displaying the files is my primary concern at this time. Editing will come later.

This mod_perl app is built around Text::Tiki (which really only turns markup into HTML). Everything else is scraped together from my other Perl projects. I've added a couple of subs that are mostly able to parse old Kwiki database files. The only things missing (for my use case, anyhow) from Text::Tiki were the headings tags and CamelCase markup... any other Kwiki-specific tags are ignored. The one compromise I had to make is that CamelCase links will only work if a leading question mark is added, like so: ?CamelCase.

## Using it

You'll want to edit the libs path in jkwiki.pl, and also you can skin the app in layout.tmpl and jkwiki.css. After that, start creating and editing the data files in the databae directory, using whatever text editor you like (until I scratch the itch, there is no web editing provided), using Text::Tiki markup and the two mentioned Kwiki constructs (headers and CamelCase). 

A sample mod_perl Apache config stanza jkwiki.conf is available.

## See it in Action

* http://joeykelly.net/scratchpad/
* http://nolug.org
